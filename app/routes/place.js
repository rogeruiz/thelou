import Ember from 'ember';
import yaml from 'npm:js-yaml';

export default Ember.Route.extend({
  setupController( controller, model ) {
    this._super(controller, model);
    this.controllerFor('place').set('rootURL', this.get('router.rootURL'))
  },
  model( params ) {
    return new Promise( ( done, /**bail*/ ) => {
      var oReq = new XMLHttpRequest();
      oReq.addEventListener( 'load', function() {
        let model = yaml.safeLoad( this.responseText, 'utf-8' );
        let place = model.places.filter( ( p ) => {
          return p.name.dasherize() === params.name
        });
        done( place );
      } );
      oReq.open('GET', `${ this.get( 'router.rootURL' ) }places.yaml`);
      oReq.send();
    } );
  }
});
