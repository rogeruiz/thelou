import Ember from 'ember';

export function uid( params ) {
  return params[ 0 ].dasherize();
}

export default Ember.Helper.helper( uid );
