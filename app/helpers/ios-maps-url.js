import Ember from 'ember';

export function iosMapsUrl(params/*, hash*/) {
  let model = params[ 0 ];
  let q = model.name.replace( /\s/g, '+' );
  let sll = model.location.replace( /\s/g, '' );
  let mapsUrl = `http://maps.apple.com/?q=${ q }&sll=${ sll }&z=15&t=r`;
  return mapsUrl;
}

export default Ember.Helper.helper(iosMapsUrl);
