import Ember from 'ember';

export default Ember.Component.extend({
  actions: {
    showPlace( place ) {
      let name = place.name.dasherize();
      this.get( 'router' ).transitionTo( 'place', name );
    },
    followHref( url ) {
      window.open( url );
      return false;
    },
  },
});
