import Ember from 'ember';
import yaml from 'npm:js-yaml';

export default Ember.Route.extend( {
  setupController( controller, model ) {
    this._super(controller, model);
    this.controllerFor('application').set('rootURL', this.get('router.rootURL'))
  },
  model() {
    return new Promise( ( done, /**bail*/ ) => {
      var oReq = new XMLHttpRequest();
      oReq.addEventListener( 'load', function() {
        let model = yaml.safeLoad( this.responseText, 'utf-8' );
        done( model );
      } );
      oReq.open('GET', `${ this.get( 'router.rootURL' ) }places.yaml`);
      oReq.send();
    } );
  }
} );
