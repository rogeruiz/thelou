import Ember from 'ember';
import config from './config/environment';

const Router = Ember.Router.extend({
  location: config.locationType,
  rootURL: config.rootURL,
  location: 'hash'
});

Router.map(function() {
  this.route('index', {
    path: '/',
  });
  this.route('place', {
    path: '/place/:name',
  });
});

export default Router;
